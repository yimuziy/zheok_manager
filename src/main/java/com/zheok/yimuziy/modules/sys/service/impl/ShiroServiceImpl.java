/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.zheok.yimuziy.modules.sys.service.impl;

import com.zheok.yimuziy.common.utils.Constant;
import com.zheok.yimuziy.modules.app.dao.UserDao;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.sys.dao.SysMenuDao;
import com.zheok.yimuziy.modules.sys.dao.SysUserDao;
import com.zheok.yimuziy.modules.sys.dao.SysUserTokenDao;
import com.zheok.yimuziy.modules.sys.entity.SysMenuEntity;
import com.zheok.yimuziy.modules.sys.entity.SysUserEntity;
import com.zheok.yimuziy.modules.sys.entity.SysUserTokenEntity;
import com.zheok.yimuziy.modules.sys.service.ShiroService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ShiroServiceImpl implements ShiroService {
    @Autowired
    private SysMenuDao sysMenuDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private SysUserTokenDao sysUserTokenDao;

    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //系统管理员，拥有最高权限
        if(userId == Constant.SUPER_ADMIN){
            List<SysMenuEntity> menuList = sysMenuDao.selectList(null);
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            permsList = userDao.queryAllPerms(userId);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    @Override
    public SysUserTokenEntity queryByToken(String token) {
        return sysUserTokenDao.queryByToken(token);
    }

    @Override
    public UserEntity queryUser(Long userId) {
        return userDao.selectById(userId);
    }
}
