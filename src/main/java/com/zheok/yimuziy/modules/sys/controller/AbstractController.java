

package com.zheok.yimuziy.modules.sys.controller;

import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.sys.entity.SysUserEntity;
import org.apache.catalina.User;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Controller公共组件
 *
 * @author Mark sunlightcs@gmail.com
 */
public abstract class AbstractController {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	protected UserEntity getUser() {
//
		return (UserEntity) SecurityUtils.getSubject().getPrincipal();

	}

	protected Long getUserId() {
		return getUser().getId().longValue() ;
	}
}
