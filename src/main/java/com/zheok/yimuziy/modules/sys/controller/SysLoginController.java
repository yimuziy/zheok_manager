/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.zheok.yimuziy.modules.sys.controller;

import com.zheok.yimuziy.common.utils.DeviceUtil;
import com.zheok.yimuziy.common.utils.GetIPUtils;
import com.zheok.yimuziy.common.utils.IPUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.SignService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.sys.entity.SysUserEntity;
import com.zheok.yimuziy.modules.sys.form.SysLoginForm;
import com.zheok.yimuziy.modules.sys.service.SysCaptchaService;
import com.zheok.yimuziy.modules.sys.service.SysUserService;
import com.zheok.yimuziy.modules.sys.service.SysUserTokenService;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;

/**
 * 登录相关
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
public class SysLoginController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;

	@Autowired
	private UserService userService;

	@Autowired
	private SysUserTokenService sysUserTokenService;
	@Autowired
	private SysCaptchaService sysCaptchaService;
	@Autowired
	private SignService signService;

	/**
	 * 验证码
	 */
	@GetMapping("captcha.jpg")
	public void captcha(HttpServletResponse response, String uuid)throws IOException {
		response.setHeader("Cache-Control", "no-store, no-cache");
		response.setContentType("image/jpeg");

		//获取图片验证码
		BufferedImage image = sysCaptchaService.getCaptcha(uuid);

		ServletOutputStream out = response.getOutputStream();
		ImageIO.write(image, "jpg", out);
		IOUtils.closeQuietly(out);
	}

	/**
	 * 登录
	 */
	@PostMapping("/sys/login")
	public Map<String, Object> login(@RequestBody SysLoginForm form, HttpServletRequest request, Device device)throws IOException {
		boolean captcha = sysCaptchaService.validate(form.getUuid(), form.getCaptcha());
		if(!captcha){
			return R.error("验证码不正确");
		}

		//用户信息
		UserEntity user = userService.queryByUserName(form.getUsername());

		//账号不存在、密码错误
		if(user == null || !user.getUserPassword().equals(new Sha256Hash(form.getPassword(), user.getUserSalt()).toHex())) {
			return R.error("账号或密码不正确");
		}

//		//账号锁定
//		if(user.getStatus() == 0){
//			return R.error("账号已被锁定,请联系管理员");
//		}

		//生成token，并保存到数据库
		R r = sysUserTokenService.createToken(user.getId());
		String ipAddr = GetIPUtils.getIpAddr(request);
		String agent = DeviceUtil.getdevice(device);
		logger.info(ipAddr);
		logger.info(agent);

		logger.info(request.getHeader("USER-AGENT"));

		//TODO 可以增加对前端用户的反馈比如签到成功！
		//对登录用户进行签到
		signService.signIn(user,ipAddr,agent);



		return r;
	}


	/**
	 * 退出
	 */
	@PostMapping("/sys/logout")
	public R logout() {
		sysUserTokenService.logout(getUserId());
		return R.ok();
	}
	
}
