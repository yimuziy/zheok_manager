package com.zheok.yimuziy.modules.app.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.entity.ProjectPersonnelEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.ProjectPersonnelService;
import com.zheok.yimuziy.modules.app.service.ProjectService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.app.vo.ProjectNameVo;
import com.zheok.yimuziy.modules.app.vo.UserSimpleVo;
import com.zheok.yimuziy.modules.app.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;


/**
 * 项目人员表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/project_personnel")
public class ProjectPersonnelController {
    @Autowired
    private ProjectPersonnelService projectPersonnelService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    /**
     * 更改负责人状态
     */
    @GetMapping("/changeMaster")
    public R changeMaster(String id,String status){
        projectPersonnelService.changeMaster(Integer.parseInt(id),Integer.parseInt(status));

        return R.ok();
    }

    /**
     * 更改管理员状态
     */
    @GetMapping("/changeAdmin")
    public R changeAdmin(Integer id,Integer status){
        projectPersonnelService.changeAdmin(id,status);

        return R.ok();
    }


    /**
     * 查询所有可新增的人员信息
     */
    @GetMapping("/getAddEmployee/{id}")
    public R getAddEmployee(@PathVariable("id") Integer projectId){
        //1、查找所有可以添加的人员信息
        List<UserSimpleVo> userList = projectPersonnelService.getAddEmployee(projectId);


        //2、查找所有的项目名称和ID
        List<ProjectEntity> list = projectService.list(new QueryWrapper<ProjectEntity>().select("id", "project_name"));
        List<ProjectNameVo> collect = list.stream().map(item -> {
            ProjectNameVo projectNameVo = new ProjectNameVo();
            projectNameVo.setProjectName(item.getProjectName());
            projectNameVo.setProjectId(item.getId());
            return projectNameVo;
        }).collect(Collectors.toList());

        return  R.ok().put("userList",userList).put("projectList",list);
    }

    /**
     * 根据项目ID查看对应的人员信息
     */
    @RequestMapping("/listByProject")
    public R listByProjectId(@RequestParam Map<String, Object> params) {
        PageUtils page = projectPersonnelService.queryPageByProjectId(params, params.get("projectId"));

        if (page != null) {
            return R.ok().put("page", page);
        } else {
            return R.error("项目Id不存在");
        }
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:projectpersonnel:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = projectPersonnelService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:projectpersonnel:info")
    public R info(@PathVariable("id") Integer projectId) {
        ProjectPersonnelEntity projectPersonnel = projectPersonnelService.getById(projectId);

        //返回


        return R.ok().put("projectPersonnel", projectPersonnel);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:projectpersonnel:save")
    public R save(@RequestBody ProjectPersonnelEntity projectPersonnel) {
        projectPersonnelService.save(projectPersonnel);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:projectpersonnel:update")
    public R update(@RequestBody ProjectPersonnelEntity projectPersonnel) {
        projectPersonnelService.updateById(projectPersonnel);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:projectpersonnel:delete")
    public R delete(@RequestBody Integer[] ids) {
        projectPersonnelService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
