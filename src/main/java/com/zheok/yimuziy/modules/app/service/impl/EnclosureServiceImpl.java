package com.zheok.yimuziy.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.exception.RRException;
import com.zheok.yimuziy.common.utils.*;
import com.zheok.yimuziy.modules.app.dao.EnclosureDao;
import com.zheok.yimuziy.modules.app.entity.EnclosureEntity;
import com.zheok.yimuziy.modules.app.service.EnclosureService;
import com.zheok.yimuziy.modules.app.vo.SubjectVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service("enclosureService")
public class EnclosureServiceImpl extends ServiceImpl<EnclosureDao, EnclosureEntity> implements EnclosureService {
    @Value("${FTP.ADDRESS}")
    private String host;
    // 端口
    @Value("${FTP.PORT}")
    private int port;
    // ftp用户名
    @Value("${FTP.USERNAME}")
    private String userName;
    // ftp用户密码
    @Value("${FTP.PASSWORD}")
    private String passWord;
    // 文件在服务器端保存的主目录
    @Value("${FTP.BASEPATH}")
    private String basePath;
    //     访问图片时的基础url
    @Value("${FTP.BASEURL}")
    private String baseUrl;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EnclosureEntity> page = this.page(
                new Query<EnclosureEntity>().getPage(params),
                new QueryWrapper<EnclosureEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageBySubjectId(Map<String, Object> params) {
        QueryWrapper<EnclosureEntity> wrapper = new QueryWrapper<>();
        if (params.get("id") != null) {
            wrapper.eq("enclosure_subject_id", params.get("id"))
                    .eq("enclosure_subject_type", params.get("type"));
        }

        IPage<EnclosureEntity> page = this.page(
                new Query<EnclosureEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public String setFileName(Integer subjectId, Integer subjectType, String fileName) {
        //先根据主体ID查询所有跟主体有关的文件名
        List<EnclosureEntity> files = this.list(new QueryWrapper<EnclosureEntity>().
                eq("enclosure_subject_id", subjectId)
                .eq("enclosure_subject_type", subjectType));

//        if(StringUtils.isEmpty(fileName)){




        //返回有重复名称的集合
        List<EnclosureEntity> collect = files.stream().filter(file -> {
            //获取文件名为 xxx(1).txt 的 xxx作为相同文件的比对条件
            String name = fileName;
            if(fileName.indexOf("(") != -1){
                name = fileName.substring(0,fileName.indexOf("("));
            }else{
                name = fileName.substring(0,fileName.indexOf("."));
            }
            String oldName = file.getEnclosureName();
            if(oldName.indexOf("(") != -1){
                oldName = oldName.substring(0,oldName.indexOf("("));
            }else{
                oldName = oldName.substring(0,oldName.indexOf("."));
            }
            return name.equals(oldName);
        }).collect(Collectors.toList());

        //如果有重复的文件名，加上后缀
        if(collect.size()>0){
            String substring = fileName.substring(0, fileName.lastIndexOf("."));
            if(substring.endsWith(")")){
               return substring.substring(0,substring.length()-2)+collect.size()+")"+fileName.substring(fileName.lastIndexOf("."));
            }else{
                return substring+"("+ collect.size()+")"+fileName.substring(fileName.lastIndexOf("."));
            }
        }
//        }

        return fileName;
    }

    @Override
    public String upload(MultipartFile uploadFile, SubjectVo subjectVo) {
        if (uploadFile.isEmpty()) {
            throw new RRException("上传文件不能为空");
        }
        try {
            //1、给上传的图片生成新的文件名
            //1.1获取原始文件名
            String newName = uploadFile.getOriginalFilename();
            //1.2 解决数据库重复的文件名问题
            String dbFileName = this.setFileName(subjectVo.getSubjectId(),subjectVo.getSubjectType(), newName);

            //1.3 给服务器上传的文件名加后缀
//            String serverFileName = newName.substring(0,newName.lastIndexOf("."))+ IDUtils.genFIleName()+
//                    newName.substring(newName.lastIndexOf(".")) ;

            //获取文件类型
            String type = newName.substring(newName.lastIndexOf(".")+1).toUpperCase();

            //获取文件大小
            System.out.println(uploadFile.getSize());
            long size = uploadFile.getSize()/1000;
//            newName = newName + type;
            //1.3生成文件在服务器端存储的子目录
            String filePath = "";
            if(subjectVo.getSubjectId()!=0 && subjectVo.getSubjectType()!=0 ){
                filePath  = new String("/"+subjectVo.getSubjectType()+"/"+subjectVo.getSubjectId());
            }


            //2、把前端输入信息，包括图片的url保存到数据库
            EnclosureEntity enclosureEntity = new EnclosureEntity();

            //设置主体信息
            enclosureEntity.setEnclosureSubjectId(subjectVo.getSubjectId());
            enclosureEntity.setEnclosureSubjectName(subjectVo.getSubjectName());
            enclosureEntity.setEnclosureSubjectType(subjectVo.getSubjectType());



            enclosureEntity.setEnclosureName(dbFileName);
            enclosureEntity.setEnclosureUrl(baseUrl + filePath +"/"+dbFileName);
            enclosureEntity.setEnclosureType(type);
            enclosureEntity.setEnclosureSize(size);


            //3、把图片上传到图片服务器
            //3.1获取上传的io流
            InputStream input = uploadFile.getInputStream();

            //3.2调用FtpUtil工具类进行上传
            boolean result = FtpUtil.uploadFile(host, port, userName, passWord, basePath, filePath, dbFileName, input);


            if(result) {
                log.info("文件上传成功");
                //保存记录数
                this.save(enclosureEntity);
               return enclosureEntity.getEnclosureUrl();
            }else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }

    @Override
    public void removeEnclosure(Map<String, Object> params) {
        Integer id =  (Integer) params.get("id");
        String name = (String) params.get("name");
        String  url = (String) params.get("url");

        boolean b = FtpUtil.removeFile(host, port, userName, passWord, basePath, url, name);
        if(b){
            log.info("文件删除成功");
            this.baseMapper.delete(new QueryWrapper<EnclosureEntity>().eq("id", id));
        }else {
            log.info("文件删除失败");
        }
    }

    @Override
    public boolean rename(Integer id, Integer subjectId, String name, String url, String value) {
//        this.
//TODO 重命名以后加
        return false;
    }

}