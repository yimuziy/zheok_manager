package com.zheok.yimuziy.modules.app.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;


/**
 * 类型表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/classify")
public class ClassifyController {
    @Autowired
    private ClassifyService classifyService;

    /**
     * 获取用户类型信息
     */
    @RequestMapping("/userList")
    public R getUserClass(){
        List<ClassifyEntity> userClassify = classifyService.getUserClassify();
        return R.ok().put("userTypeList",userClassify);
    }

    /**
     * 获取项目类型信息
     */
    @RequestMapping("/projectList")
    public R getProjectClass(){
        List<ClassifyEntity> projectClassify = classifyService.getProjectClassify();
        return R.ok().put("projectTypeList", projectClassify);
    }

    /**
     * 获取任务类型信息
     */
    @RequestMapping("/taskList")
    public R getTaskClass(){
        List<ClassifyEntity> taskClassify = classifyService.getTaskClassify();
        return R.ok().put("taskTypeList",taskClassify);
    }




    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:classify:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = classifyService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:classify:info")
    public R info(@PathVariable("id") Integer id) {
            ClassifyEntity classify = classifyService.getById(id);

        return R.ok().put("classify", classify);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:classify:save")
    public R save(@RequestBody ClassifyEntity classify) {
            classifyService.save(classify);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:classify:update")
    public R update(@RequestBody ClassifyEntity classify) {
            classifyService.updateById(classify);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:classify:delete")
    public R delete(@RequestBody Integer[] ids) {
            classifyService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
