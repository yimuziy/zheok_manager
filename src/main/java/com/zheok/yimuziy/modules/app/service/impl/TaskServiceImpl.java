package com.zheok.yimuziy.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.constant.TaskStatus;
import com.zheok.yimuziy.common.utils.DateUtils;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;
import com.zheok.yimuziy.modules.app.dao.TaskDao;
import com.zheok.yimuziy.modules.app.entity.*;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.ProjectService;
import com.zheok.yimuziy.modules.app.service.TaskService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.app.vo.TaskVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;


@Service("taskService")
public class TaskServiceImpl extends ServiceImpl<TaskDao, TaskEntity> implements TaskService {

    @Autowired
    TaskDao taskDao;

    @Autowired
    UserService userService;

    @Autowired
    ClassifyService classifyService;

    @Autowired
    private ProjectService projectService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<TaskEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");
        String projectId = (String) params.get("projectId");

        if (key != null && !StringUtils.isEmpty(key)) {

            wrapper.like("task_name", key);
        }

        if (projectId != null && !projectId.isEmpty()) {
            wrapper.eq("project_Id", params.get("projectId"));
        }

        IPage<TaskEntity> page = this.page(
                new Query<TaskEntity>().getPage(params),
                wrapper
        );
        page.convert(taskEntity -> {
            TaskVo taskVo = new TaskVo();
            BeanUtils.copyProperties(taskEntity, taskVo);
            ClassifyEntity byId = classifyService.getById(taskVo.getTaskType());
            taskVo.setTaskTypeName(byId.getClassifyName());
            return taskVo;
        });

        return new PageUtils(page);
    }

    @Override
    public boolean taskReceive(Integer taskId, String explain) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setTaskStatus(TaskStatus.WORKING.getCode());
        taskEntity.setId(taskId);
        if (!StringUtils.isEmpty(explain)) {
            taskEntity.setTaskExplain(explain);
        }
        taskEntity.setTaskUpdatetime(new Date());

        return this.update(taskEntity, new QueryWrapper<TaskEntity>().eq("id", taskId));

    }


    @Override
    public boolean taskDispatch(Integer id, Integer userId, UserEntity userLogin) {
        UserEntity userEntity = userService.getById(userId);


        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setTaskStatus(TaskStatus.UNCONFIRMED.getCode());
        taskEntity.setId(id);
        taskEntity.setTaskUpdatetime(new Date());

        taskEntity.setTaskSendId(userLogin.getId());
        taskEntity.setTaskSendNo(userLogin.getUserNo());
        taskEntity.setTaskSendName(userLogin.getUserName());

        taskEntity.setTaskReceiveId(userEntity.getId());
        taskEntity.setTaskReceiveNo(userEntity.getUserNo());
        taskEntity.setTaskReceiveName(userEntity.getUserName());


        return this.update(taskEntity, new QueryWrapper<TaskEntity>().eq("id", id));


    }

    @Override
    public boolean taskRefuse(Integer id, String explain) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setTaskStatus(TaskStatus.REFUSE.getCode());
        taskEntity.setId(id);
        if (!StringUtils.isEmpty(explain)) {
            taskEntity.setTaskExplain(explain);
        }
        taskEntity.setTaskUpdatetime(new Date());

        return this.update(taskEntity, new QueryWrapper<TaskEntity>().eq("id", id));
    }

    @Override
    public boolean updateByApproval(ProgressEntity progressEntity) {
        //获取对应的任务记录
        TaskEntity byId = this.getById(progressEntity.getTaskId());
        //TODO 可以以后增加消息队列去做任务进度的更新
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(progressEntity.getTaskId());
        taskEntity.setTaskCompletion(progressEntity.getProgressNew());
        if (progressEntity.getProgressNew() == 100) {
            //任务完成更改状态
            taskEntity.setTaskStatus(TaskStatus.FINISH.getCode());
            //根据任务的完成时间计算任务
            LocalDateTime endTime = DateUtils.getDateTime(byId.getTaskEndtime());
            LocalDateTime approvalTime = DateUtils.getDateTime(progressEntity.getApprovalTime());
            long between = ChronoUnit.HOURS.between( endTime,approvalTime);

            taskEntity.setTaskOverHour(between);

            //TODO 并根据任务的完成时间调整实际的积分值 根据实际的积分值给接单后的人员尽心积分增加
        }

        taskEntity.setTaskUpdatetime(new Date());
        //保存任务的信息
        this.updateById(taskEntity);


        //重新保存项目的完成度
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(byId.getProjectId());
        projectEntity.setProjectCompletion(projectService.calculateProgress(byId.getProjectId()));
        projectService.updateById(projectEntity);

        return true;
    }


}