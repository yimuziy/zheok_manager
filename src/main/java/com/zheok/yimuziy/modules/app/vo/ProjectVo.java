package com.zheok.yimuziy.modules.app.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * @author: ywz
 * @createDate: 2021/4/1
 * @description:
 */
@Data
public class ProjectVo {
    /**
     * 主键  项目ID


     */
    @TableId
    private Integer id;
    /**
     * 项目名称


     */
    private String projectName;
    /**
     * 项目完成进度【0-100】
     */
    private Integer projectCompletion;
    /**
     * 项目完成状态 【 0-进行中、1-暂停中、2-已结束、3-已删除（非物理删除）】
     */
    private Integer projectStatus;
    /**
     * 关联类型表

     */
    private Integer projectType;
    /**
     * 项目重要性 [0-5]
     */
    private Double projectImportance;
    /**
     * 项目创建时间
     */
    private Date projectStarttime;
    /**
     * 项目需要截止的时间

     */
    private Date projectEndtime;
    /**
     * 任务暂停时间或者该项目某任务完成的时间
     */
    private Date projectUpdatetime;

    /**
     * 项目类型名称

     */
    private String projectTypeName;

}

