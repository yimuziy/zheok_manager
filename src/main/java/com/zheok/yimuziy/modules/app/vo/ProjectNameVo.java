package com.zheok.yimuziy.modules.app.vo;

import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/4/8
 * @description:
 */
@Data
public class ProjectNameVo {

    private Integer projectId;
    private String projectName;
}
