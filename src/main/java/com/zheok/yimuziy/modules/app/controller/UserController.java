package com.zheok.yimuziy.modules.app.controller;

import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.sys.controller.AbstractController;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * 用户表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:42:14
 */
@RestController
@RequestMapping("/zheok/user")
public class UserController extends AbstractController {
    @Autowired
    private UserService userService;

    @Autowired
    private ClassifyService classifyService;


    /**
     * 获取所有人员
     */
    @GetMapping("/queryUsers")
    public R queryUsers() {
        List<UserEntity> users = userService.list();
        return R.ok().put("users", users);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = userService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:user:info")
    public R info(@PathVariable("id") Integer id) {
        UserEntity user = userService.getById(id);
        //同时返回用户类型
        List<ClassifyEntity> userClassify = classifyService.getUserClassify();

        return R.ok().put("user", user).put("userTypeList", userClassify);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:user:save")
    public R save(@RequestBody UserEntity user) {
//            userService.save(user);

//        ValidatorUtils.validateEntity(user, AddGroup.class);

//        user.setCreateUserId(getUserId());
        Integer integer = userService.saveUser(user);
        if (integer == 0) {
            return R.ok();
        } else if (integer == 1) {
            return R.error("用户工号或手机号重复");
        } else {
            return R.error("未知异常，请于管理员联系");
        }


    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:user:update")
    public R update(@RequestBody UserEntity user) {
        userService.updateById(user);
//        ValidatorUtils.validateEntity(user, UpdateGroup.class);

//        user.setCreateUserId(getUserId());
        userService.update(user);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:user:delete")
    public R delete(@RequestBody Integer[] userIds) {

        if (ArrayUtils.contains(userIds, 1L)) {
            return R.error("系统管理员不能删除");
        }

        if (ArrayUtils.contains(userIds, getUserId())) {
            return R.error("当前用户不能删除");
        }
        userService.deleteBatch(userIds);

        return R.ok();
    }

}
