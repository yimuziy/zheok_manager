package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

    import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 签到表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("sign")
public class SignEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            /**
         * 签到表主键
         */
                @TableId
            private Integer id;
            /**
         * 用户ID


         */
            private Integer userId;
            /**
         * 用户工号
         */
            private String userNo;
            /**
         * 用户姓名
         */
            private String userName;
            /**
         * 终端信息：格式  IP_AGENT
         */
            private String signIpAgent;
            /**
         * 签到时间

         */
            private Date signTime;
            /**
         * 是否迟到
         */
            private Integer signIdLate;
    
}
