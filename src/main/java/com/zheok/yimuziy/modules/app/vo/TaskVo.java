package com.zheok.yimuziy.modules.app.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * @author: ywz
 * @createDate: 2021/4/1
 * @description:
 */
@Data
public class TaskVo {
    /**
     * 任务ID，主键
     */
    @TableId
    private Integer id;
    /**
     * 项目ID

     */
    private Integer projectId;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 任务名称

     */
    private String taskName;
    /**
     * 任务摘要

     */
    private String taskAbstract;
    /**
     * 任务重要性：【0-5】
     */
    private Double taskImportance;
    /**
     * 任务完成进度 【0-100】
     */
    private Integer taskCompletion;
    /**
     * 任务积分
     */
    private Integer taskScore;
    /**
     * 任务类型

     */
    private Integer taskType;

    /**
     * 任务类型名称

     */
    private String taskTypeName;
    /**
     * 派单人员ID
     */
    private Integer taskSendId;
    /**
     * 派单人员工号
     */
    private String taskSendNo;
    /**
     * 派单人员姓名

     */
    private String taskSendName;
    /**
     * 接单人员ID
     */
    private Integer taskReceiveId;
    /**
     * 接单人员工号
     */
    private String taskReceiveNo;
    /**
     * 接单人员姓名
     */
    private String taskReceiveName;
    /**
     * 【0-派单中 、1-未确认、2-执行中、3-已完成、4-不合理】派单后，状态为：未确认，接单人接单后为：执行中， 不合理的、需要改派的：不合理
     */
    private Integer taskStatus;
    /**
     * 备注，如果拒单，拒单备注添加在这里
     */
    private String taskExplain;
    /**
     * 任务需要开始的时间，默认创建即开始，也可设定开始时间

     */
    private Date taskStarttime;
    /**
     * 任务需要截止的时间
     */
    private Date taskEndtime;
    /**
     * 任务被update的时间
     */
    private Date taskUpdatetime;

    /**
     * 超时的小时数
     */
    private Long taskOverHour;



}

