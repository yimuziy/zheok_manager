package com.zheok.yimuziy.modules.app.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/4/1
 * @description:
 */
@Data
public class UserVo {
    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     * 用户工号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 用户性别
     */
    private String userGender;
    /**
     * 用户手机号
     */
    private String userPhone;
    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 用户类型--关联classify表
     */
    private Integer userType;
    /**
     * 用户类型--关联classify表
     */
    private String userTypeName;

    /**
     * 用户密码
     */
    private String userPassword;
    /**
     * 用户积分
     */
    private Integer userScore;

    /**
     * 盐
     */
    private String userSalt;

}

