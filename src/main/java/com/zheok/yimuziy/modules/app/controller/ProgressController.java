package com.zheok.yimuziy.modules.app.controller;

import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.ProgressEntity;
import com.zheok.yimuziy.modules.app.service.ProgressService;
import com.zheok.yimuziy.modules.app.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;


/**
 * 任务进度表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/progress")
public class ProgressController {
    @Autowired
    private ProgressService progressService;

    @Autowired
    private TaskService taskService;


    /**
     * 任务进度审核
     */
    @PostMapping("/progressApproval")
    public R progressApproval(@RequestBody ProgressEntity progressEntity){
        progressService.progressApproval(progressEntity);

        return R.ok();
    }


    /**
     * 根据任务Id查询当前任务的进度
     */
    @GetMapping("/infoByTaskId")
    public R infoByTaskId(@RequestParam Map<String, Object> params) {
        PageUtils page = progressService.infoByTaskId(params);


        return R.ok().put("page", page);
    }

//    /**
//     * 提交进度
//     */
//    @RequestMapping("/taskSubmit")
//    public R taskSubmit(@RequestParam Map<String, Object> params){
////        progressService.taskSubmit(Integer id)
//
//        return R.ok();
//    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:progress:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = progressService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:progress:info")
    public R info(@PathVariable("id") Integer id) {
        ProgressEntity progress = progressService.getById(id);

        return R.ok().put("progress", progress);
    }

    /**
     * 保存 提交任务
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:progress:save")
    public R save(@RequestBody ProgressEntity progress) {



        progressService.save(progress);
        //根据任务进度信息更新任务完成度
//        taskService.updateByProgress(progress);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:progress:update")
    public R update(@RequestBody ProgressEntity progress) {


        progressService.updateById(progress);
//        if (progress.getProgressNew() != null) {
//            //根据任务进度信息更新任务完成度
//            taskService.updateByProgress(progress);
//        }


        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:progress:delete")
    public R delete(@RequestBody Integer[] ids) {
        progressService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
