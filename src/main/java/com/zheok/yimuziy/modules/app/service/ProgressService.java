package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.ProgressEntity;

import java.util.List;
import java.util.Map;

/**
 * 任务进度表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface ProgressService extends IService<ProgressEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据任务Id查看相应的任务进度
     */
    PageUtils infoByTaskId(Map<String, Object> params);
    /**
     * 任务审核
     */
    void  progressApproval(ProgressEntity progressEntity);
}

