package com.zheok.yimuziy.modules.app.service.impl;

import com.zheok.yimuziy.common.constant.ProjectStatus;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.entity.TaskEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.ProjectPersonnelService;
import com.zheok.yimuziy.modules.app.service.TaskService;
import com.zheok.yimuziy.modules.app.vo.ProjectNameVo;
import com.zheok.yimuziy.modules.app.vo.ProjectVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;

import com.zheok.yimuziy.modules.app.dao.ProjectDao;
import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.service.ProjectService;


@Service("projectService")
public class ProjectServiceImpl extends ServiceImpl<ProjectDao, ProjectEntity> implements ProjectService {

    @Autowired
    private ProjectPersonnelService personnelService;

    @Autowired
    private ClassifyService classifyService;

    @Autowired
    private TaskService taskService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper<ProjectEntity> wrapper = new QueryWrapper<>();
        String key = (String) params.get("key");

        if (!key.isEmpty() && key != null) {
            wrapper.like("project_name", key);
        }


        IPage<ProjectEntity> page = this.page(
                new Query<ProjectEntity>().getPage(params),
                wrapper
        );

        page.convert(projectEntity -> {
            ProjectVo projectVo = new ProjectVo();
            BeanUtils.copyProperties(projectEntity,projectVo);
            ClassifyEntity byId = classifyService.getById(projectVo.getProjectType());
            projectVo.setProjectTypeName(byId.getClassifyName());
            return projectVo;
        });
        return new PageUtils(page);
    }

    @Override
    public void projectCreate(ProjectEntity projectEntity) {
        projectEntity.setProjectStatus(ProjectStatus.WORKING.getCode());
        save(projectEntity);
    }

    @Override
    public void projectSuspend(ProjectEntity projectEntity) {
        projectEntity.setProjectStatus(ProjectStatus.SUSPEND.getCode());
        projectEntity.setProjectUpdatetime(new Date());
        boolean id = update(projectEntity, new QueryWrapper<ProjectEntity>().eq("id", projectEntity.getId()));
    }

    @Override
    public void projectFinish(ProjectEntity projectEntity) {
        projectEntity.setProjectStatus(ProjectStatus.CLOSE.getCode());
        projectEntity.setProjectUpdatetime(new Date());
        boolean id = update(projectEntity, new QueryWrapper<ProjectEntity>().eq("id", projectEntity.getId()));
    }

    @Override
    public void projectDelete(ProjectEntity projectEntity) {
        projectEntity.setProjectStatus(ProjectStatus.DELETE.getCode());
        projectEntity.setProjectUpdatetime(new Date());
        boolean id = update(projectEntity, new QueryWrapper<ProjectEntity>().eq("id", projectEntity.getId()));
    }

    @Override
    public void addProjectPersonnel(Integer projectId, Integer[] userIds) {

    }

    @Override
    public void projectStart(ProjectEntity projectEntity) {
        projectEntity.setProjectStatus(ProjectStatus.WORKING.getCode());
        projectEntity.setProjectUpdatetime(new Date());
        boolean id = update(projectEntity, new QueryWrapper<ProjectEntity>().eq("id", projectEntity.getId()));
    }

    @Override
    public Integer calculateProgress(Integer projectId) {
        //获取该任务的所有任务信息并整理需要的数据
        List<TaskEntity> taskEntities = taskService.list(new QueryWrapper<TaskEntity>().eq("project_id", projectId));
        //统计所有任务的重要性总和
        AtomicReference<Integer> allImportance = new AtomicReference<>(0);
        AtomicReference<Integer> nowImportance = new AtomicReference<>(0);
        //统计所有
//        List<TaskCompletionVo> taskList = taskEntities.stream().map(item -> {
//            TaskCompletionVo taskCompletionVo = new TaskCompletionVo();
//            taskCompletionVo.setTaskId(item.getId());
//            taskCompletionVo.setTaskCompletion(item.getTaskCompletion());
//            taskCompletionVo.setTaskImportance(item.getTaskImportance());
//            taskCompletionVo.setProjectId(item.getProjectId());
//            allImportance.updateAndGet(v -> v + item.getTaskImportance());
//
//            return taskCompletionVo;
//        }).collect(Collectors.toList());
        taskEntities.forEach(item ->{
            allImportance.updateAndGet(v -> v + item.getTaskImportance().intValue());
            nowImportance.updateAndGet(v -> v + item.getTaskImportance().intValue() * item.getTaskCompletion());
        });

        //  3  5  6 7     21    2100   30   50
        //根据所有的任务计算出项目的完成进度
        DecimalFormat df = new DecimalFormat("0");
        String format = df.format(nowImportance.get() / ((Integer) (allImportance.get() * 100)).doubleValue());


         double progress = nowImportance.get() / ((Integer) (allImportance.get() * 100)).doubleValue();


        return (int)( progress * 100);
    }

    @Override
    public List<ProjectNameVo> getProjectNameVos() {
        List<ProjectEntity> list = this.list(new QueryWrapper<ProjectEntity>().select("id", "project_name"));
        List<ProjectNameVo> collect = list.stream().map(item -> {
            ProjectNameVo projectNameVo = new ProjectNameVo();
            projectNameVo.setProjectName(item.getProjectName());
            projectNameVo.setProjectId(item.getId());
            return projectNameVo;
        }).collect(Collectors.toList());
        return collect;
    }
}