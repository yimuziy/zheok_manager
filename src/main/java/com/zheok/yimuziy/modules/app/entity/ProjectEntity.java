package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

    import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 项目表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("project")
public class ProjectEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            /**
         * 主键  项目ID


         */
                @TableId
            private Integer id;
            /**
         * 项目名称


         */
            private String projectName;
            /**
         * 项目完成进度【0-100】
         */
            private Integer projectCompletion;
            /**
         * 项目完成状态 【 0-进行中、1-暂停中、2-已结束、3-已删除（非物理删除）】
         */
            private Integer projectStatus;
            /**
         * 关联类型表

         */
            private Integer projectType;
            /**
         * 项目重要性 [0-5]
         */
            private Double projectImportance;
            /**
         * 项目创建时间
         */
            private Date projectStarttime;
            /**
         * 项目需要截止的时间

         */
            private Date projectEndtime;
            /**
         * 任务暂停时间或者该项目某任务完成的时间
         */
            private Date projectUpdatetime;
    
}
