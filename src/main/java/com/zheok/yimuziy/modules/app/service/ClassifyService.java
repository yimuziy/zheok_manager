package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;

import java.util.List;
import java.util.Map;

/**
 * 类型表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface ClassifyService extends IService<ClassifyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取所有用户类型
     */
    List<ClassifyEntity>  getUserClassify();

    /**
     * 获取所有任务类型
     */
    List<ClassifyEntity>  getTaskClassify();

    /**
     * 获取所有项目类型
     */
    List<ClassifyEntity>  getProjectClassify();
}

