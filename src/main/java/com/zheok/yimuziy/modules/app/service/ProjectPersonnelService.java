package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.ProjectPersonnelEntity;
import com.zheok.yimuziy.modules.app.vo.UserSimpleVo;

import java.util.List;
import java.util.Map;

/**
 * 项目人员表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface ProjectPersonnelService extends IService<ProjectPersonnelEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 添加项目人员
     */
    void addProjectPersonnel(Integer ProjectId, List<ProjectPersonnelEntity> projectPersonnelEntity);

    /**
     * 根据项目ID查看对应的人员信息
     * @param params
     * @param projectId
     * @return
     */
    PageUtils queryPageByProjectId(Map<String, Object> params, Object projectId);

    /**
     * 查找可以添加到项目的人员信息
     * @param projectId
     * @return
     */
    List<UserSimpleVo> getAddEmployee(Integer projectId);

    /**
     * 更改负责人状态
     * @param id
     * @param status
     */
    void changeMaster(Integer id,Integer status);


    /**
     * 更改管理员状态
     * @param id
     * @param status
     */
    void changeAdmin(Integer id,Integer status);
}

