package com.zheok.yimuziy.modules.app.service.impl;

import com.zheok.yimuziy.common.constant.ClassifyType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;

import com.zheok.yimuziy.modules.app.dao.ClassifyDao;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;


@Service("classifyService")
public class ClassifyServiceImpl extends ServiceImpl<ClassifyDao, ClassifyEntity> implements ClassifyService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ClassifyEntity> page = this.page(
                new Query<ClassifyEntity>().getPage(params),
                new QueryWrapper<ClassifyEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public List<ClassifyEntity> getUserClassify() {
        return this.list(new QueryWrapper<ClassifyEntity>().eq("classify_type", ClassifyType.USER.getCode()));
    }

    @Override
    public List<ClassifyEntity> getTaskClassify() {
        return this.list(new QueryWrapper<ClassifyEntity>().eq("classify_type", ClassifyType.TASK.getCode()));
    }

    @Override
    public List<ClassifyEntity> getProjectClassify() {
        return this.list(new QueryWrapper<ClassifyEntity>().eq("classify_type", ClassifyType.PROJECT.getCode()));
    }


}