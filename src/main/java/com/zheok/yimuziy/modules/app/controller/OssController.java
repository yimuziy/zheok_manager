package com.zheok.yimuziy.modules.app.controller;

import cn.hutool.core.date.DateTime;
import com.zheok.yimuziy.common.exception.RRException;
import com.zheok.yimuziy.common.utils.FtpUtil;
import com.zheok.yimuziy.common.utils.IDUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.EnclosureEntity;
import com.zheok.yimuziy.modules.app.service.EnclosureService;
import com.zheok.yimuziy.modules.app.vo.SubjectVo;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * @author: ywz
 * @createDate: 2021/4/2
 * @description:
 */
@Controller()
@RequestMapping("/oss")
public class OssController {



    @Autowired
    EnclosureService enclosureService;

    @RequestMapping("/upload")
    @ResponseBody
    public R pictureUpload(
//            @RequestParam Map<String, Object> params){
            @RequestParam("file") MultipartFile uploadFile, SubjectVo subjectVo   ){
//            @RequestParam("file") MultipartFile uploadFile, @RequestParam("subjectObject") Map<String,Object> param){
//            @RequestParam("username")String username,
//            @RequestParam("password") String password) {
//        MultipartFile uploadFile = n;
        String upload = enclosureService.upload(uploadFile, subjectVo);
        if(!StringUtils.isEmpty(upload)){
            return R.ok().put("url",upload);
        }else{
            return R.error("文件上传失败");
        }
    }





}
