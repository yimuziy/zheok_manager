package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.sys.entity.SysUserEntity;

import java.util.Map;

/**
 * 用户表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:42:14
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据用户名，查询系统用户
     * @param username
     * @return
     */
    UserEntity queryByUserName(String username);

    /**
     * 修改密码
     * @param userId       用户ID
     * @param password     原密码
     * @param newPassword  新密码
     */
    boolean updatePassword(Integer userId, String password, String newPassword);

    /**
     * 保存用户
     */
    Integer saveUser(UserEntity user);

    /**
     * 修改用户
     */
    void update(UserEntity user);

    /**
     * 删除用户
     */
    void deleteBatch(Integer[] userIds);

    
}

