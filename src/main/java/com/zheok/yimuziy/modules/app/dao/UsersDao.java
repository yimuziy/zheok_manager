

package com.zheok.yimuziy.modules.app.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zheok.yimuziy.modules.app.entity.UsersEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface UsersDao extends BaseMapper<UsersEntity> {

}
