package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.EnclosureEntity;
import com.zheok.yimuziy.modules.app.vo.SubjectVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 附件表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface EnclosureService extends IService<EnclosureEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据主体Id查询对应附件
     * @param params
     * @return
     */
    PageUtils queryPageBySubjectId(Map<String, Object> params);

    /**
     * 设置文件名
     *
     */
     String setFileName (Integer subjectId, Integer subjectType, String fileName);


    /**
     * 上传文件
     * @param uploadFile
     * @param subjectVo
     * @return 文件的Url
     */
    String    upload (MultipartFile uploadFile, SubjectVo subjectVo);

    /**
     * 删除记录
     * @param params
     */
    void removeEnclosure(Map<String, Object> params);

    /**
     * 重命名
     * @param id
     * @param name
     * @param url
     * @param value
     * @return
     */
    boolean rename(Integer id, Integer subjectId, String name, String url, String value);
}

