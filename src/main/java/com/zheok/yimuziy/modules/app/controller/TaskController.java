package com.zheok.yimuziy.modules.app.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zheok.yimuziy.common.constant.TaskStatus;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.ProjectService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.app.vo.ProjectNameVo;
import com.zheok.yimuziy.modules.sys.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zheok.yimuziy.modules.app.entity.TaskEntity;
import com.zheok.yimuziy.modules.app.service.TaskService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;


/**
 * 任务表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/task")
public class TaskController extends AbstractController {
    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ClassifyService classifyService;

    @Autowired
    private UserService userService;


    /**
     * 新增任务初始化需要
     */
    @GetMapping("/taskAddInit")
    public R taskAddInit(){
        List<ProjectEntity> list = projectService.list(new QueryWrapper<ProjectEntity>().select("id", "project_name"));
        List<ProjectNameVo> collect = list.stream().map(item -> {
            ProjectNameVo projectNameVo = new ProjectNameVo();
            projectNameVo.setProjectName(item.getProjectName());
            projectNameVo.setProjectId(item.getId());
            return projectNameVo;
        }).collect(Collectors.toList());

        List<ClassifyEntity> taskClassify = classifyService.getTaskClassify();


        return R.ok().put("projectNameList",collect).put("taskTypeList",taskClassify);
    }

    /**
     * 接单
     */
    @PostMapping("/taskReceive")
    public R taskReceive(@RequestBody Map<String ,String > params){

        boolean result =  taskService.taskReceive(Integer.parseInt(params.get("id")),"");

        if(result){
            return R.ok("拒单成功");
        }else {
            return R.error("拒单失败");
        }
    }


    /**
     * 派分订单
     */
    @PostMapping("/taskDispatch")
    public R taskDispatch(@RequestBody Map<String ,String > params){
        Integer id = Integer.parseInt(params.get("id"));
        Integer userId = Integer.parseInt(params.get("userId"));

        boolean result =  taskService.taskDispatch(id,userId,getUser());

        if(result){
            return R.ok("派单成功");
        }else {
            return R.error("派单失败");
        }
    }


    /**
     * 拒单
     */
    @PostMapping("/taskRefuse")
    public R taskRefuse(@RequestBody Map<String, String> params ){
        boolean result =  taskService.taskRefuse(Integer.parseInt(params.get("id")),params.get("value"));

        if(result){
            return R.ok("拒单成功");
        }else {
            return R.error("拒单失败");
        }
    }


    /**
     * 根据项目ID查询列表
     */
    @RequestMapping("/listByProjectId")
    //@RequiresPermissions("app:task:list")
    public R listByProjectId(@RequestParam Map<String, Object> params) {
        PageUtils page = taskService.queryPage(params);

        List<ProjectNameVo> projectNameVos = projectService.getProjectNameVos();

        return R.ok().put("page", page).put("projectNameList",projectNameVos);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:task:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = taskService.queryPage(params);

        List<ProjectNameVo> projectNameVos = projectService.getProjectNameVos();

        return R.ok().put("page", page).put("projectNameList",projectNameVos);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:task:info")
    public R info(@PathVariable("id") Integer id) {
            TaskEntity task = taskService.getById(id);
            //返回所有的任务类型
        List<ProjectEntity> list = projectService.list(new QueryWrapper<ProjectEntity>().select("id", "project_name"));
        List<ProjectNameVo> collect = list.stream().map(item -> {
            ProjectNameVo projectNameVo = new ProjectNameVo();
            projectNameVo.setProjectName(item.getProjectName());
            projectNameVo.setProjectId(item.getId());
            return projectNameVo;
        }).collect(Collectors.toList());

        List<ClassifyEntity> taskClassify = classifyService.getTaskClassify();
        //TODO 设置到底是项目的人员还是说所有的人员信息
        List<UserEntity> userList = userService.list();

        return R.ok().put("task", task).put("projectNameList",collect).put("taskTypeList",taskClassify).put("userList",userList);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:task:save")
    public R save(@RequestBody TaskEntity task) {
        task.setTaskUpdatetime(new Date());
        task.setTaskStatus(TaskStatus.CREATE.getCode());

        taskService.save(task);

        //更新项目完成度
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(task.getProjectId());
        projectEntity.setProjectCompletion(projectService.calculateProgress(task.getProjectId()));
        projectService.updateById(projectEntity);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:task:update")
    public R update(@RequestBody TaskEntity task) {
            taskService.updateById(task);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:task:delete")
    public R delete(@RequestBody Integer[] ids) {
            taskService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
