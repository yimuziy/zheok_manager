package com.zheok.yimuziy.modules.app.vo;

import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/4/6
 * @description:
 *  主体类型对应信息
 */
@Data
public class SubjectVo {

    private Integer subjectId;

    private Integer subjectType;

    private String subjectName;
}
