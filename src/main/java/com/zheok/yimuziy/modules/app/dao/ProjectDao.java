package com.zheok.yimuziy.modules.app.dao;

import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 项目表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Mapper
public interface ProjectDao extends BaseMapper<ProjectEntity> {

}
