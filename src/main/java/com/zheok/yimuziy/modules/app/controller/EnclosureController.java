package com.zheok.yimuziy.modules.app.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zheok.yimuziy.modules.app.entity.EnclosureEntity;
import com.zheok.yimuziy.modules.app.service.EnclosureService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;


/**
 * 附件表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/enclosure")
public class EnclosureController {
    @Autowired
    private EnclosureService enclosureService;


    /**
     * 重命名
     */
    @PostMapping("/rename")
    public R taskRefuse(@RequestBody Map<String, String> params ){
        Integer id = 0;
        Integer subjectId = 0;
        String name = "";
        String url = "";
        String value = "";
        if(params.get("id")!=null){
            id = Integer.parseInt(params.get("id"));
        }
        if(params.get("subjectId")!=null){
            subjectId = Integer.parseInt(params.get("subjectId"));
        }
        if(params.get("name")!=null){
            name = params.get("name");
        }
        if(params.get("url")!=null){
            url = params.get("url");
        }
        if(params.get("value")!=null){
            value = params.get("value");
        }

        boolean result =  enclosureService.rename(id,subjectId,name,url,value);

        if(result){
            return R.ok("拒单成功");
        }else {
            return R.error("拒单失败");
        }
    }

    /**
     * 根据主体ID查看对应的附件信息
     */

    @RequestMapping("/queryBySubjectId")
    public R queryBySubjectId(@RequestParam Map<String, Object> params){
        PageUtils page = enclosureService.queryPageBySubjectId(params);

        return R.ok().put("page",page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:enclosure:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = enclosureService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:enclosure:info")
    public R info(@PathVariable("id") Integer id) {
            EnclosureEntity enclosure = enclosureService.getById(id);

        return R.ok().put("enclosure", enclosure);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:enclosure:save")
    public R save(@RequestBody EnclosureEntity enclosure) {
            enclosureService.save(enclosure);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:enclosure:update")
    public R update(@RequestBody EnclosureEntity enclosure) {
            enclosureService.updateById(enclosure);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:enclosure:delete")
    public R delete(@RequestBody Map<String,Object> params) {
//            enclosureService.removeByIds(Arrays.asList(ids));
        enclosureService.removeEnclosure(params);
        return R.ok();
    }



}
