package com.zheok.yimuziy.modules.app.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/4/11
 * @description:
 */
@Data
public class UserSimpleVo {
    /**
     * 主键
     */
    private Integer id;
    /**
     * 用户工号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;
}
