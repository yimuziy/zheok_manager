

package com.zheok.yimuziy.modules.app.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.modules.app.entity.UsersEntity;
import com.zheok.yimuziy.modules.app.form.LoginForm;

/**
 * 用户
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface UsersService extends IService<UsersEntity> {

	UsersEntity queryByMobile(String mobile);

	/**
	 * 用户登录
	 * @param form    登录表单
	 * @return        返回用户ID
	 */
	long login(LoginForm form);
}
