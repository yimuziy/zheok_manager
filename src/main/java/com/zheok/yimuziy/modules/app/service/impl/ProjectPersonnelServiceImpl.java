package com.zheok.yimuziy.modules.app.service.impl;

import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.ProjectService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.app.vo.PersonnelVo;
import com.zheok.yimuziy.modules.app.vo.UserSimpleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;

import com.zheok.yimuziy.modules.app.dao.ProjectPersonnelDao;
import com.zheok.yimuziy.modules.app.entity.ProjectPersonnelEntity;
import com.zheok.yimuziy.modules.app.service.ProjectPersonnelService;


@Service("projectPersonnelService")
public class ProjectPersonnelServiceImpl extends ServiceImpl<ProjectPersonnelDao, ProjectPersonnelEntity> implements ProjectPersonnelService {

    @Autowired
    ProjectService projectService;

    @Autowired
    UserService userService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProjectPersonnelEntity> page = this.page(
                new Query<ProjectPersonnelEntity>().getPage(params),
                new QueryWrapper<ProjectPersonnelEntity>()
        );
        IPage<PersonnelVo> convert = page.convert(personnel -> {
            ProjectEntity projectEntity = projectService.getById(personnel.getProjectId());
            PersonnelVo personnelVo = new PersonnelVo();
            UserEntity byId = userService.getById(personnel.getUserId());

            personnelVo.setProjectId(personnel.getProjectId());
            personnelVo.setProjectName(projectEntity.getProjectName());

            personnelVo.setUserId(byId.getId());
            personnelVo.setUserName(byId.getUserName());
            personnelVo.setUserNo(byId.getUserNo());
            personnelVo.setIsAdmin(personnel.getIsAdmin());
            personnelVo.setIsMaster(personnel.getIsMaster());
            return personnelVo;
        });

        return new PageUtils(convert);
    }

    @Override
    public void addProjectPersonnel( Integer projectId, List<ProjectPersonnelEntity> projectPersonnelEntites) {

        //构建要返回的数据
        List<PersonnelVo> personnelVos = projectPersonnelEntites.stream().map(personnel -> {
            ProjectEntity projectEntity = projectService.getById(personnel.getProjectId());
            PersonnelVo personnelVo = new PersonnelVo();
            UserEntity byId = userService.getById(personnel.getUserId());

            personnelVo.setProjectId(personnel.getProjectId());
            personnelVo.setProjectName(projectEntity.getProjectName());

            personnelVo.setUserId(byId.getId());
            personnelVo.setUserName(byId.getUserName());
            personnelVo.setUserName(byId.getUserNo());
            personnelVo.setIsAdmin(personnel.getIsAdmin());
            personnelVo.setIsMaster(personnel.getIsMaster());

            return personnelVo;
        }).collect(Collectors.toList());

    }

    @Override
    public PageUtils queryPageByProjectId(Map<String, Object> params, Object projectId) {
        if(projectId!=null) {
            IPage<ProjectPersonnelEntity> page = this.page(
                    new Query<ProjectPersonnelEntity>().getPage(params),
                    new QueryWrapper<ProjectPersonnelEntity>().eq("project_id", projectId)
            );
            IPage<PersonnelVo> convert = page.convert(personnel -> {
                ProjectEntity projectEntity = projectService.getById(personnel.getProjectId());
                PersonnelVo personnelVo = new PersonnelVo();
                UserEntity byId = userService.getById(personnel.getUserId());

                personnelVo.setId(personnel.getId());
                personnelVo.setProjectId(personnel.getProjectId());
                personnelVo.setProjectName(projectEntity.getProjectName());

                personnelVo.setUserId(byId.getId());
                personnelVo.setUserName(byId.getUserName());
                personnelVo.setUserNo(byId.getUserNo());
                personnelVo.setIsAdmin(personnel.getIsAdmin());
                personnelVo.setIsMaster(personnel.getIsMaster());
                return personnelVo;
            });
            return new PageUtils(convert);
        }else {
            return null;
        }


    }

    @Override
    public List<UserSimpleVo> getAddEmployee(Integer projectId) {
        ProjectPersonnelDao baseMapper = this.baseMapper;
        List<UserEntity> addEmployee = baseMapper.getAddEmployee(projectId);
        List<UserSimpleVo> collect = addEmployee.stream().map(item -> {
            UserSimpleVo userSimpleVo = new UserSimpleVo();
            BeanUtils.copyProperties(item, userSimpleVo);
            return userSimpleVo;
        }).collect(Collectors.toList());


        return collect;
    }

    @Override
    public void changeMaster(Integer id, Integer status) {
        ProjectPersonnelEntity projectPersonnelEntity = new ProjectPersonnelEntity();
        projectPersonnelEntity.setId(id);
        projectPersonnelEntity.setIsMaster(status);

        this.updateById(projectPersonnelEntity);
    }

    @Override
    public void changeAdmin(Integer id, Integer status) {
        ProjectPersonnelEntity projectPersonnelEntity = new ProjectPersonnelEntity();
        projectPersonnelEntity.setId(id);
        projectPersonnelEntity.setIsAdmin(status);

        this.updateById(projectPersonnelEntity);
    }

}