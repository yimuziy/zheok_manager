package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.vo.ProjectNameVo;

import java.util.List;
import java.util.Map;

/**
 * 项目表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface ProjectService extends IService<ProjectEntity> {

    PageUtils queryPage(Map<String, Object> params);


    /**
     * 项目的创建
     *
     * @param projectEntity
     */
    void projectCreate(ProjectEntity projectEntity);

    /**
     * 暂停项目
     */
    void projectSuspend(ProjectEntity projectEntity);

    /**
     * 完成项目
     */
    void projectFinish(ProjectEntity projectEntity);

    /**
     * 删除项目（非物理删除）
     */
    void projectDelete(ProjectEntity projectEntity);

    /**
     * 添加项目人员
     */
    void addProjectPersonnel(Integer projectId, Integer[] userIds);


    /**
     * 继续项目
     * @param projectEntity
     */
    void projectStart(ProjectEntity projectEntity);

    /**
     * 任务进度修改后调用
     * 计算项目进度
     * @return
     */
    Integer calculateProgress(Integer projectId );

    /**
     * 获取所有项目Id和名称
     * @return
     */
    List<ProjectNameVo> getProjectNameVos();
}

