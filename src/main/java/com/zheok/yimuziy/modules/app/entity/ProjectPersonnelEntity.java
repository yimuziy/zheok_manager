package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

    import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 项目人员表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("project_personnel")
public class ProjectPersonnelEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            /**
         * 主键

         */
                @TableId
            private Integer id;
            /**
         * 项目ID
         */
            private Integer projectId;
            /**
         * 用户id
         */
            private Integer userId;
            /**
         * 是否为负责人 【0-不是、1-是】
         */
            private Integer isMaster;
            /**
         * 是否为管理员: 【0-不是、1-是】
         */
            private Integer isAdmin;
    
}
