package com.zheok.yimuziy.modules.app.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/3/31
 * @description:
 */
@Data
public class PersonnelVo {
    @TableId
    private Integer id;
    /**
     * 项目ID
     */
    private Integer projectId;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 是否为负责人 【0-不是、1-是】
     */
    private Integer isMaster;
    /**
     * 是否为管理员: 【0-不是、1-是】
     */
    private Integer isAdmin;

    /**
     * 用户工号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 项目名称
     */
    private String projectName;
}
