package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.SignEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;

import java.util.Map;

/**
 * 签到表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface SignService extends IService<SignEntity> {

    PageUtils queryPage(Map<String, Object> params);


    void  signIn(UserEntity userId, String ipAddr, String agent);
}

