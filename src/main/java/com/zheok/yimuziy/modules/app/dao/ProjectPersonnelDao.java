package com.zheok.yimuziy.modules.app.dao;

import com.zheok.yimuziy.modules.app.entity.ProjectPersonnelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 项目人员表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Mapper
public interface ProjectPersonnelDao extends BaseMapper<ProjectPersonnelEntity> {

    List<UserEntity>  getAddEmployee(@Param("projectId") Integer projectId);

}
