package com.zheok.yimuziy.modules.app.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zheok.yimuziy.modules.app.entity.SignEntity;
import com.zheok.yimuziy.modules.app.service.SignService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;


/**
 * 签到表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/sign")
public class SignController {
    @Autowired
    private SignService signService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:sign:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = signService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:sign:info")
    public R info(@PathVariable("id") Integer id) {
            SignEntity sign = signService.getById(id);

        return R.ok().put("sign", sign);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:sign:save")
    public R save(@RequestBody SignEntity sign) {
            signService.save(sign);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:sign:update")
    public R update(@RequestBody SignEntity sign) {
            signService.updateById(sign);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:sign:delete")
    public R delete(@RequestBody Integer[] ids) {
            signService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
