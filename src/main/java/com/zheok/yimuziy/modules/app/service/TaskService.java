package com.zheok.yimuziy.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.modules.app.entity.ProgressEntity;
import com.zheok.yimuziy.modules.app.entity.TaskEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;

import java.util.Map;

/**
 * 任务表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
public interface TaskService extends IService<TaskEntity> {

    PageUtils queryPage(Map<String, Object> params);


    /**
     * 接单
     */
    boolean taskReceive(Integer taskId,String explain);

    /**
     * 派单
     * @param id
     * @param userId
     * @param userEntity
     * @return
     */
    boolean taskDispatch(Integer id, Integer userId, UserEntity userEntity);


    /**
     * 拒单
     * @param id
     * @return
     */
    boolean taskRefuse(Integer id,String explain);


    /**
     * 根据任务进度更新任务的完成度
     */
    boolean updateByApproval(ProgressEntity progressEntity);
}

