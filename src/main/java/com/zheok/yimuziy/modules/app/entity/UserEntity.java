package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:42:14
 */
@Data
@TableName("user")
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     * 用户工号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 用户性别
     */
    private String userGender;
    /**
     * 用户手机号
     */
    private String userPhone;
    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 用户类型--关联classify表
     */
    private Integer userType;
    /**
     * 用户密码
     */
    private String userPassword;
    /**
     * 用户积分
     */
    private Integer userScore;

    /**
     * 盐
     */
    private String userSalt;

    /**
     * 角色ID列表
     */
    @TableField(exist=false)
    private List<Long> roleIdList;


}
