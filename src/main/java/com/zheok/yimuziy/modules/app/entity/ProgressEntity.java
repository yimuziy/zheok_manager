package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务进度表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("progress")
public class ProgressEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务进度主键
     */
    @TableId
    private Integer id;
    /**
     * 任务id
     */
    private Integer taskId;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 用户工号
     */
    private String userNo;
    /**
     * 用户姓名
     */
    private String userName;
    /**
     * 用户完成的进度 【 0-100】
     */
    private Integer progressNew;
    /**
     * 任务完成情况描述：如果过多请上传附件
     */
    private String progressCompleteDescription;
    /**
     * 待处理的工作描述，如果过多请上传附件
     */
    private String progressImperfectDescription;

    /**
     * 0-审核中、1-审核通过、2-审核失败
     */
    private Integer approvalStatus;

    /**
     * 审核原因
     */
    private String approvalReason;

    /**
     * 审核日期
     */
    private Date approvalTime;


}
