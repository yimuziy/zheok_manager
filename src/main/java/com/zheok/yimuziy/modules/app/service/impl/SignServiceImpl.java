package com.zheok.yimuziy.modules.app.service.impl;

import com.zheok.yimuziy.modules.app.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;

import com.zheok.yimuziy.modules.app.dao.SignDao;
import com.zheok.yimuziy.modules.app.entity.SignEntity;
import com.zheok.yimuziy.modules.app.service.SignService;


@Service("signService")
public class SignServiceImpl extends ServiceImpl<SignDao, SignEntity> implements SignService {



    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SignEntity> page = this.page(
                new Query<SignEntity>().getPage(params),
                new QueryWrapper<SignEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void signIn(UserEntity user, String ipAddr, String agent) {
        //根据用户id查询用户最新的签到记录
       SignEntity signEntity = this.getOne(new QueryWrapper<SignEntity>().
                eq("user_id", user.getId()).
                orderByDesc("sign_time"),false);

        if( signEntity!=null){
            //签到记录不为空查询最近一次的签到日期是否为今日
            Date signTime = signEntity.getSignTime();
            //获取之前签到的日期
            LocalDate localDate = signTime.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate now = LocalDate.now();
//            System.out.println(now);
//            System.out.println(localDate);
            if(now.toString().equals(localDate.toString())){
                //为True代表签到日期为今日，不用签到
                return;
            }else{
                //为Fasle代表签到日期不为今日，需要签到
                SignEntity newSign = new SignEntity();
                newSign.setUserId(user.getId());
                newSign.setUserName(user.getUserName());
                newSign.setUserNo(user.getUserNo());
                newSign.setSignTime(new Date());
                //TODO 迟到判断
                newSign.setSignIdLate(0);
                newSign.setSignIpAgent(ipAddr+"_"+agent);

                this.save(newSign);
            }


        }else{
            //签到记录为空，记录签到日期
            SignEntity newSign = new SignEntity();
            newSign.setUserId(user.getId());
            newSign.setUserName(user.getUserName());
            newSign.setUserNo(user.getUserNo());
            newSign.setSignTime(new Date());
            //TODO 迟到判断
            newSign.setSignIdLate(0);
            newSign.setSignIpAgent(ipAddr+"_"+agent);

            this.save(newSign);
        }



    }



}