
package com.zheok.yimuziy.modules.app.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.exception.RRException;
import com.zheok.yimuziy.common.validator.Assert;
import com.zheok.yimuziy.modules.app.dao.UsersDao;
import com.zheok.yimuziy.modules.app.entity.UsersEntity;
import com.zheok.yimuziy.modules.app.form.LoginForm;
import com.zheok.yimuziy.modules.app.service.UsersService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;


@Service("usersService")
public class UsersServiceImpl extends ServiceImpl<UsersDao, UsersEntity> implements UsersService {

	@Override
	public UsersEntity queryByMobile(String mobile) {
		return baseMapper.selectOne(new QueryWrapper<UsersEntity>().eq("mobile", mobile));
	}

	@Override
	public long login(LoginForm form) {
		UsersEntity user = queryByMobile(form.getMobile());
		Assert.isNull(user, "手机号或密码错误");

		//密码错误
		if(!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))){
			throw new RRException("手机号或密码错误");
		}

		return user.getUserId();
	}
}
