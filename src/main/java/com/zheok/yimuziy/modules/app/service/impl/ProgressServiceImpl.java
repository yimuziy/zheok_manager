package com.zheok.yimuziy.modules.app.service.impl;

import com.zheok.yimuziy.modules.app.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;

import com.zheok.yimuziy.modules.app.dao.ProgressDao;
import com.zheok.yimuziy.modules.app.entity.ProgressEntity;
import com.zheok.yimuziy.modules.app.service.ProgressService;


@Service("progressService")
public class ProgressServiceImpl extends ServiceImpl<ProgressDao, ProgressEntity> implements ProgressService {

    @Autowired
    private TaskService taskService;



    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProgressEntity> page = this.page(
                new Query<ProgressEntity>().getPage(params),
                new QueryWrapper<ProgressEntity>().orderByDesc("id")
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils infoByTaskId(Map<String, Object> params) {
//        List<ProgressEntity> task_id = list(new QueryWrapper<ProgressEntity>().eq("task_id", taskId));
        IPage<ProgressEntity> page = this.page(
                new Query<ProgressEntity>().getPage(params),
                new QueryWrapper<ProgressEntity>().eq("task_id", params.get("taskId"))
        );
        return new PageUtils(page);
    }

    @Override
    public void progressApproval(ProgressEntity progressEntity) {
        //设置审核时间
        progressEntity.setApprovalTime(new Date());
        if(progressEntity.getApprovalStatus()==1){
            //审核通过需要更新任务以及任务进度
            this.updateById(progressEntity);
            ProgressEntity byId = this.getById(progressEntity.getId());
            taskService.updateByApproval(byId);
        }else{
            //未通过审核保存信息
            this.updateById(progressEntity);
        }
    }


}