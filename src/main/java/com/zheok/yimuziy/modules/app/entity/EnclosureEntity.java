package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

    import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 附件表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("enclosure")
public class EnclosureEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            /**
         * 附件ID
         */
                @TableId
            private Integer id;
            /**
         * 类型主体：【0-用户、1-项目、2-任务 】
         */
            private Integer enclosureSubjectType;
            /**
         * 主体ID
         */
            private Integer enclosureSubjectId;
            /**
         * 主体名称
         */
            private String enclosureSubjectName;
            /**
         * 附件名称

         */
            private String enclosureName;
            /**
         * 附件RUL：http://xxxxx
         */
            private String enclosureUrl;
            /**
         * 附件类型： DOC、PPT、XLS
         */
            private String enclosureType;
            /**
         * 附件大小：单位kb
         */
            private Long enclosureSize;
    
}
