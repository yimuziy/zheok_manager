package com.zheok.yimuziy.modules.app.dao;

import com.zheok.yimuziy.modules.app.entity.SignEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 签到表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Mapper
public interface SignDao extends BaseMapper<SignEntity> {

}
