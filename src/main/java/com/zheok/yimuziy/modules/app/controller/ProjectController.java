package com.zheok.yimuziy.modules.app.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.R;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.entity.ProjectEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.ProjectService;
import com.zheok.yimuziy.modules.app.vo.ProjectNameVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 项目表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@RestController
@RequestMapping("/zheok/project")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @Autowired
    private ClassifyService classifyService;


    /**
     * 获取所有项目Id和名称
     */
    @GetMapping("/queryProjectNameList")
    public R queryProjectNameList() {
        List<ProjectNameVo> collect = projectService.getProjectNameVos();

        return R.ok().put("projectNameList", collect);
    }


    /**
     * 暂停项目
     */
    @GetMapping("/suspend/{id}")
    public R projectSuspend(@PathVariable("id") Integer id) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(id);
        projectService.projectSuspend(projectEntity);
        return R.ok();
    }


    /**
     * 完成项目
     */
    @GetMapping("/finish/{id}")
    public R projectFinish(@PathVariable("id") Integer id) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(id);
        projectService.projectFinish(projectEntity);
        return R.ok();
    }


    /**
     * 删除项目
     */
    @GetMapping("/delete/{id}")
    public R projectDelete(@PathVariable("id") Integer id) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(id);
        projectService.projectDelete(projectEntity);
        return R.ok();
    }

    /**
     * 继续项目
     */
    @GetMapping("/start/{id}")
    public R projectStart(@PathVariable("id") Integer id) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(id);
        projectService.projectStart(projectEntity);
        return R.ok();
    }

    /**
     * 添加项目人员
     */
    @GetMapping("/addProjectPersonnel")
    public R addProjectPersonnel(@PathVariable("id") Integer id) {

        return R.ok();
    }


    /**
     * 添加项目附件
     */
    @GetMapping("/addEnlosure")
    public R addEnlosure() {
        return R.ok();
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("app:project:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = projectService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("app:project:info")
    public R info(@PathVariable("id") Integer id) {
        ProjectEntity project = projectService.getById(id);
        //同时加入项目的所有类型
        List<ClassifyEntity> projectClassify = classifyService.getProjectClassify();


        return R.ok().put("project", project).put("projectTypeList", projectClassify);
    }


    /**
     * 新增项目
     */
    @RequestMapping("/save")
    // @RequiresPermissions("app:project:save")
    public R save(@RequestBody ProjectEntity project) {
        projectService.save(project);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("app:project:update")
    public R update(@RequestBody ProjectEntity project) {
        projectService.updateById(project);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("app:project:delete")
    public R delete(@RequestBody Integer[] ids) {
        projectService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
