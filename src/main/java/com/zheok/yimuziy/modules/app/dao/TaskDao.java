package com.zheok.yimuziy.modules.app.dao;

import com.zheok.yimuziy.modules.app.entity.TaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 任务表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Mapper
public interface TaskDao extends BaseMapper<TaskEntity> {

}
