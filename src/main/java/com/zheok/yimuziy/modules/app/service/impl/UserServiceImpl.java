package com.zheok.yimuziy.modules.app.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zheok.yimuziy.common.exception.RRException;
import com.zheok.yimuziy.common.utils.Constant;
import com.zheok.yimuziy.common.utils.PageUtils;
import com.zheok.yimuziy.common.utils.Query;
import com.zheok.yimuziy.modules.app.dao.UserDao;
import com.zheok.yimuziy.modules.app.entity.ClassifyEntity;
import com.zheok.yimuziy.modules.app.entity.UserEntity;
import com.zheok.yimuziy.modules.app.service.ClassifyService;
import com.zheok.yimuziy.modules.app.service.UserService;
import com.zheok.yimuziy.modules.app.vo.UserVo;
import com.zheok.yimuziy.modules.sys.entity.SysUserEntity;
import com.zheok.yimuziy.modules.sys.service.SysRoleService;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

    @Autowired
    SysRoleService sysRoleService;

    @Autowired
    ClassifyService classifyService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<UserEntity> userEntityQueryWrapper = new QueryWrapper<>();
        if (!StringUtils.isBlank((String) params.get("key"))) {
            String key = (String) params.get("key");
            userEntityQueryWrapper.like("user_no", key).or().like("user_name", key).or().like("user_phone", key);
        }

        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                userEntityQueryWrapper
        );

        //转换为使用的User对象 （包含类型名称）
        page.convert(userEntity -> {
            UserVo userVo = new UserVo();
            BeanUtils.copyProperties(userEntity, userVo);
            ClassifyEntity byId = classifyService.getById(userVo.getUserType());
            userVo.setUserTypeName(byId.getClassifyName());
            return userVo;
        });


        return new PageUtils(page);
    }

    @Override
    public UserEntity queryByUserName(String username) {
        return baseMapper.queryByUserName(username);
    }

    @Override
    public boolean updatePassword(Integer userId, String password, String newPassword) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserPassword(newPassword);
        return this.update(userEntity,
                new QueryWrapper<UserEntity>().eq("id", userId).eq("user_password", password));
    }

    @Override
    public Integer saveUser(UserEntity user) {

        List<UserEntity> list = this.list(new QueryWrapper<UserEntity>().
                eq("user_no", user.getUserNo()).
                or().
                eq("user_phone", user.getUserPhone()));
        if(list.size()>0){
            return 1;
        }

        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        user.setUserPassword(new Sha256Hash(user.getUserPassword(), salt).toHex());
        user.setUserSalt(salt);
        this.save(user);

        return 0;
        //检查角色是否越权
//        checkRole(user);

        //保存用户与角色关系
//        sysUserRoleService.saveOrUpdate(user.getId(), user.getRoleIdList());

    }

    @Override
    public void update(UserEntity user) {
        if (StringUtils.isBlank(user.getUserPassword())) {
            user.setUserPassword(null);
        } else {
            String salt = RandomStringUtils.randomAlphanumeric(20);
            user.setUserPassword(new Sha256Hash(user.getUserPassword(), salt).toHex());
            user.setUserSalt(salt);
        }
        this.updateById(user);

        //检查角色是否越权
//        checkRole(user);

        //保存用户与角色关系
//        sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
    }

    @Override
    public void deleteBatch(Integer[] userIds) {
        this.removeByIds(Arrays.asList(userIds));
    }


    /**
     * 检查角色是否越权
     */
    private void checkRole(SysUserEntity user) {
        if (user.getRoleIdList() == null || user.getRoleIdList().size() == 0) {
            return;
        }
        //如果不是超级管理员，则需要判断用户的角色是否自己创建
        if (user.getCreateUserId() == Constant.SUPER_ADMIN) {
            return;
        }

        //查询用户创建的角色列表
        List<Long> roleIdList = sysRoleService.queryRoleIdList(user.getCreateUserId());

        //判断是否越权
        if (!roleIdList.containsAll(user.getRoleIdList())) {
            throw new RRException("新增用户所选角色，不是本人创建");
        }
    }

}