package com.zheok.yimuziy.modules.app.vo;

import lombok.Data;

/**
 * @author: ywz
 * @createDate: 2021/4/11
 * @description:
 */
@Data
public class TaskCompletionVo {

    /**
     * 任务ID，主键
     */
    private Integer taskId;

     /** 项目ID，主键
         */
    private Integer projectId;


    /**
     * 任务重要性
     */
    private Integer taskImportance;

    /**
     * 任务完成进度 【0-100】
     */
    private Integer taskCompletion;
}
