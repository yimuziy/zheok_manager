package com.zheok.yimuziy.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

    import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 类型表
 *
 * @author yimuziy
 * @email yimuziy@gmail.com
 * @date 2021-03-30 11:29:08
 */
@Data
@TableName("classify")
public class ClassifyEntity implements Serializable {
    private static final long serialVersionUID = 1L;

            /**
         * 主键
         */
                @TableId
            private Integer id;
            /**
         * 类型主体：【0-用户、1-项目、2-任务 】
         */
            private Integer classifyType;
            /**
         * 类型名称
         */
            private String classifyName;
            /**
         * 类型说明

         */
            private String classifyExplain;
    
}
