package com.zheok.yimuziy.common.constant;

/**
 * @author: ywz
 * @createDate: 2021/3/30
 * @description:
 */
public enum ProjectStatus {

    WORKING(0,"进行中"),
    SUSPEND(1,"暂停中"),
    CLOSE(2,"已结束"),
    DELETE(3,"已删除");




    ProjectStatus(int code,String msg){
        this.code=code;
        this.msg = msg;
    }


    private int code;
    private String msg;


    public int getCode() {
        return code;
    }

    public java.lang.String getMsg() {
        return msg;
    }
}
