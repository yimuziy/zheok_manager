package com.zheok.yimuziy.common.constant;

/**
 * @author: ywz
 * @createDate: 2021/3/30
 * @description:
 */
public enum TaskStatus {
    CREATE(0,"派分中"),
    UNCONFIRMED(1,"未确认"),
    WORKING(2,"执行中"),
    FINISH(3,"已完成"),
    REFUSE(4,"已拒绝");





    TaskStatus(int code,String msg){
        this.code=code;
        this.msg = msg;
    }

    private int code;
    private String msg;


    public int getCode() {
        return code;
    }

    public java.lang.String getMsg() {
        return msg;
    }
}
