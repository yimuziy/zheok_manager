package com.zheok.yimuziy.common.constant;

/**
 * @author: ywz
 * @createDate: 2021/3/30
 * @description:
 */
public enum ClassifyType {

    USER(0,"用户"),
    PROJECT(1,"项目"),
    TASK(2,"任务");





    ClassifyType(int code,String msg){
        this.code=code;
        this.msg = msg;
    }


    private int code;
    private String msg;


    public int getCode() {
        return code;
    }

    public java.lang.String getMsg() {
        return msg;
    }
}
