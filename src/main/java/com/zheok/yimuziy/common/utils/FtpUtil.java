package com.zheok.yimuziy.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.beans.factory.annotation.Value;

import java.io.*;

/**
 * @author: ywz
 * @createDate: 2021/4/2
 * @description: ftp上传下载工具
 */
@Slf4j
public class FtpUtil {

//    @Value("${FTP.BASEPATH}")
//    private static String basePath;



    /**
     * Description: 向FTP服务器上传文件
     *
     * @param host     FTP服务器hostname
     * @param port     FTP服务器端口
     * @param username FTP登录账号
     * @param password FTP登录密码
     * @param basePath FTP服务器基础目录,/zheok/file
     * @param filePath FTP服务器文件存放路径。例如分日期存放：/2015/01/01。文件的路径为basePath+filePath
     * @param filename 上传到FTP服务器上的文件名
     * @param input    输入流
     * @return 成功返回true，否则返回false
     */
    public static boolean uploadFile(String host, int port, String username, String password, String basePath,
                                     String filePath, String filename, InputStream input) {
        boolean result = false;
        FTPClient ftp = new FTPClient();
        try {
            int reply;
            ftp.connect(host, port);// 连接FTP服务器
            // 如果采用默认端口，可以使用ftp.connect(host)的方式直接连接FTP服务器
            boolean login = ftp.login(username, password);// 登录
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                return result;
            }
//            ftp.setControlEncoding("UTF-8");
            //切换到上传目录
            if (!ftp.changeWorkingDirectory(basePath + filePath)) {
                //如果目录不存在创建目录
                String[] dirs = filePath.split("/");
                String tempPath = basePath;
                for (String dir : dirs) {
                    if (null == dir || "".equals(dir)) continue;
                    tempPath += "/" + dir;
                    if (!ftp.changeWorkingDirectory(tempPath)) {
                        if (!ftp.makeDirectory(tempPath)) {
                            return result;
                        } else {
                            ftp.changeWorkingDirectory(tempPath);
                        }
                    }
                }
            }
            //设置为被动模式
            ftp.enterLocalPassiveMode();
            //设置上传文件的类型为二进制类型
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            //上传文件
            if (!ftp.storeFile(new String(filename.getBytes("UTF-8"), "iso-8859-1"), input)) {
                return result;
            }
            input.close();
            ftp.logout();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException ioe) {
                }
            }
        }
        return result;
    }

    /**
     * Description: 从FTP服务器更改文件名
     *
     * @param host        FTP服务器hostname
     * @param port        FTP服务器端口
     * @param username    FTP登录账号
     * @param password    FTP登录密码
     * @param basePath   FTP服务器基础目录,/zheok/file
     * @param urlPath    url地址
     * @param srcFname    原来的名字
     * @param targetFname 新名字
     * @return
     */
    public static boolean renameFile(String host, int port, String username, String password,String basePath ,String urlPath, String srcFname,
                                        String targetFname) {
        boolean result = false;
        FTPClient ftp = new FTPClient();

        try {
            int reply;
            ftp.connect(host, port);
            // 如果采用默认端口，可以使用ftp.connect(host)的方式直接连接FTP服务器
            ftp.login(username, password);// 登录
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                return result;
            }
            //设置为被动模式
            ftp.enterLocalPassiveMode();
            ftp.rename(getRemotePath(basePath,urlPath) + "/" + srcFname, getRemotePath(basePath,urlPath) + "/" + targetFname);


            ftp.logout();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException ioe) {
                }
            }
        }
        return result;
    }

    /**
     * Description: 从FTP服务器删除文件
     *
     * @param host        FTP服务器hostname
     * @param port        FTP服务器端口
     * @param username    FTP登录账号
     * @param password    FTP登录密码
     * @param basePath   FTP服务器基础目录,/zheok/file
     * @param urlPath    url地址
     * @param srcFname    文件名字
     * @return
     */
    public static boolean removeFile(String host, int port, String username, String password,String basePath ,String urlPath, String srcFname) {
        boolean result = false;
        FTPClient ftp = new FTPClient();

        try {
            int reply;
            ftp.connect(host, port);
            // 如果采用默认端口，可以使用ftp.connect(host)的方式直接连接FTP服务器
            ftp.login(username, password);// 登录
            reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                return result;
            }
            //设置为被动模式
            ftp.enterLocalPassiveMode();
            ftp.deleteFile(getRemotePath(basePath, urlPath) + "/" + srcFname);


            ftp.logout();
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException ioe) {
                }
            }
        }
        return result;
    }


    /**
     * 测试代码
     */
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream(new File("/Users/yimuziy/Desktop/docker-data/nginx-zheok/nginx.conf"));
            boolean flag = uploadFile("127.0.0.1", 21, "root", "root", "/zheok/", "file", "abc2.png", in);
            System.out.println(flag);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取相对路径的方法
     */
    public static String getRemotePath(String basePath ,String path) {
        if (path.indexOf(basePath) != -1) {
            return path.substring(path.indexOf(basePath), path.lastIndexOf("/"));
        }
        return null;
    }


    public static String changeFileName(String fileName) {
        return fileName.replace(" ", "%20")
                .replace("/", "%2f")
                .replace("?", "%3f")
                .replace("%", "%25")
                .replace("#", "%23")
                .replace("&", "%26");
    }
}
