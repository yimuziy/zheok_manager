package com.zheok.yimuziy.test;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.boot.test.context.SpringBootTest;

import javax.security.auth.callback.CallbackHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author: ywz
 * @createDate: 2021/4/2
 * @description:
 */

public class Test {
    public static void main(String[] args) throws IOException {
        FTPClient client = new FTPClient();

        //IP地址和端口号    写在登录之前
        client.connect("127.0.0.1", 21);
        //用户名和密码
        client.login("roor", "root");
        //设置文件类型
        client.setFileType(FTPClient.BINARY_FILE_TYPE);

        //本机图片
        FileInputStream is = new FileInputStream("/Users/yimuziy/Desktop/docker-data/nginx-zheok/nginx.conf");


        //设置上传目录  需要实现创建好文件，并赋予权限
        client.changeWorkingDirectory("/zheok/file");

        //存储时的名称
        client.storeFile("nginx.conf",is);
        //退出
        client.logout();

        //同名文件上传会覆盖原有内容
        System.out.println("上传成功");


    }

    @org.junit.jupiter.api.Test
    public void test01(){
        String fileName = "dsd(1).txt";
        System.out.println(fileName.substring(0,fileName.indexOf("(")));
        String substring = fileName.substring(0, fileName.lastIndexOf("."));
        System.out.println(substring);
        if(substring.endsWith(")")){
            System.out.println(substring.substring(0, substring.length() - 2)  + "3)");
        }else{
            System.out.println(substring + "(3)" + fileName.substring(fileName.lastIndexOf(".")));
        }
    }

    @org.junit.jupiter.api.Test
    public void test02(){
        String path = "http://127.0.0.1:88/zheok/file/2/1/新建 RTF 文档552.rtf";
        String basePath = "/zheok/file";
        if(path.indexOf(basePath)!=-1){
            System.out.println(path.substring(path.indexOf(basePath),path.lastIndexOf("/")));
        }
    }

    @org.junit.jupiter.api.Test
    public void test03(){
        LocalDate now = LocalDate.now();
        System.out.println(now);
        Date date = new Date();
        System.out.println(now);
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        System.out.println(localDate);


    }


    @org.junit.Test
    public void test04() throws ParseException {
        //项目截止日期
        Date newDate = new Date();

        //实际完成日期
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date endDate = simpleDateFormat.parse("2021-04-13 9:30:48");


        System.out.println(ChronoUnit.HOURS.between(LocalDateTime.now(), getDateTime(endDate)));

//        System.out.println(newDate);
//        System.out.println(endDate);
    }

    public LocalDateTime getDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();

       return instant.atZone(zoneId).toLocalDateTime();
    }

}
